proxy = require('proxy-middleware')
serveStatic = require('serve-static')
httpPlease = require('connect-http-please')
url = require('url')
middlewares = require('./speed-middleware')

module.exports = (grunt) ->
  pkg = grunt.file.readJSON('package.json')
  accountName = process.env.VTEX_ACCOUNT or pkg.accountName or 'basedevmkp'
  environment = process.env.VTEX_ENV or 'vtexcommercestable'
  secureUrl = process.env.VTEX_SECURE_URL or pkg.secureUrl
  verbose = grunt.option('verbose')

  if secureUrl
    imgProxyOptions = url.parse("https://#{accountName}.vteximg.com.br/arquivos")
  else 
    imgProxyOptions = url.parse("http://#{accountName}.vteximg.com.br/arquivos")

  imgProxyOptions.route = '/arquivos'

  # portalHost is also used by connect-http-please
  # example: basedevmkp.vtexcommercestable.com.br
  portalHost = "#{accountName}.#{environment}.com.br"
  if secureUrl
    portalProxyOptions = url.parse("https://#{portalHost}/")
  else
    portalProxyOptions = url.parse("http://#{portalHost}/")
  portalProxyOptions.preserveHost = true
  portalProxyOptions.cookieRewrite = "${accountName}.vtexlocal.com.br"

  rewriteLocation = (location) ->
    return location
      .replace('https:', 'http:')
      .replace(environment, 'vtexlocal')

  config =
    clean:
      everything:
        src: ['build']
      emptyfolders: 
        options: 
          force: true
        files: [{
            src: ['build/**/*', '!build/**/*.css']
            filter: (path) -> return grunt.file.isDir(path) && require('fs').readdirSync(path).length == 0;
          }
          { src: ['build/src', 'src/js/templates'] }
        ]

    copy:
      main:
        files: [
          {
            src: ['src/css/**/*.css']
            dest: 'build/'
          }
          {
            expand: true
            cwd: 'src'
            src: ['templates/**/*', '!templates/dust/**/*.dust', 'portal/**/*']
            dest: 'build'
          }
        ]

    cssmin:
      main:
        expand: true
        cwd: 'build/'
        src: ['**/*.css', '!**/*.min.css', '!**/style.css']
        dest: 'build/'
        ext: '.css'

    uglify:
      main:
        expand: true,
        cwd: 'build/',
        src: 'portal/**/*.js',
        dest: 'build/'
        ext: '.js'

    requirejs:
      compile:
        options:
          baseUrl: 'src/js'
          dir: "build/arquivos/"
          optimizeCss: "none"
          removeCombined: true
          writeBuildTxt: false
          paths: {
            common: 'modules/bm-common'
            menu: 'modules/bm-menu'
            search: 'modules/bm-search'
            cart: 'modules/bm-cart'
            flexslider: 'vendor/jquery.flexslider-min'
            template: 'templates/bm-template'
          }
          modules: [
            { name: 'common' }
            {
              name: 'pages/bm-homepage'
              include: ['pages/bm-homepage']
              exclude: ['common']
            }
            {
              name: 'pages/bm-catalog'
              include: ['pages/bm-catalog']
              exclude: ['common']
            }
            {
              name: 'pages/bm-product'
              include: ['pages/bm-product']
              exclude: ['common']
            }
            {
              name: 'pages/bm-account'
              include: ['pages/bm-account']
              exclude: ['common']
            }
            {
              name: 'pages/bm-orders'
              include: ['pages/bm-orders']
              exclude: ['common']
            }
            {
              name: 'pages/bm-about'
              include: ['pages/bm-about']
              exclude: ['common']
            }
            {
              name: 'pages/bm-contact'
              include: ['pages/bm-contact']
              exclude: ['common']
            }
          ]

    move:
      compile:
        src: ['build/arquivos/**/*.js', 'build/src/css/**/*.css']
        dest: 'build/arquivos/'

    dust: 
      compile:
        files: [
          expand: true,
          cwd: 'src/templates/dust/',
          src: '**/*.dust',
          dest: 'src/js/templates',
          ext: '.js'
        ]
      options:
        relative: true
        runtime: false

    sass:
      options: 
        outputStyle: 'compressed'
        sourceMap: false
      main:
        files: ['build/arquivos/bm-style.css': 'src/sass/bm-style.sass']

    connect:
      http:
        options:
          hostname: "*"
          livereload: true
          port: process.env.PORT || 80
          middleware: [
            middlewares.disableCompression
            middlewares.rewriteLocationHeader(rewriteLocation)
            middlewares.replaceHost(portalHost)
            middlewares.replaceHtmlBody(environment, accountName, secureUrl)
            httpPlease(host: portalHost, verbose: verbose)
            serveStatic('./build')
            proxy(imgProxyOptions)
            proxy(portalProxyOptions)
            middlewares.errorHandler
          ]

    watch:
      options:
        livereload: true
      files: ['src/**/*']
      tasks: ['build']

  tasks =
    # Building block tasks
    build: ['clean:everything', 'copy', 'cssmin', 'uglify', 'dust', 'requirejs', 'move', 'sass', 'clean:emptyfolders']
    # Deploy tasks
    dist: ['build'] # Dist - minifies files
    test: []
    # Development tasks
    default: ['build', 'connect', 'watch']
    devmin: ['build', 'connect:http:keepalive'] # Minifies files and serve

  # Project configuration.
  grunt.config.init config
  if grunt.cli.tasks[0] is 'sass'
    grunt.loadNpmTasks 'grunt-sass'
  else if grunt.cli.tasks[0] is 'coffee'
    grunt.loadNpmTasks 'grunt-contrib-coffee'
  else
    grunt.loadNpmTasks name for name of pkg.devDependencies when name[0..5] is 'grunt-'
  grunt.registerTask taskName, taskArray for taskName, taskArray of tasks
