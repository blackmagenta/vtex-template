# VTEX Template

A VTEX starter template.

This is a copy of [VTEX Speed](https://github.com/vtex/speed) with a few modifications:

- Switch from less to sass;
- Removed spritesmith;
- Added requirejs;
- Added Dustjs;
- Changed initial template files;

## How to use

Just like the original vtex speed:

Clone this repository or download it, change the `accountName` on `package.json` to the name of your working vtex account (e.g mystore);

```json
    {
        "name": "vtex-template",
        "accountName": "mystore",
    }
```

Install dependencies by running `npm install` and then run `npm start`:

```shell
    cd vtex-template
    npm install
    npm start
```

Open your browser at `localhost` to authenticate and start developing.